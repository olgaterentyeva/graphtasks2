package com.company.graph2;

import java.util.*;

public class GraphTasks2Solution implements GraphTasks2 {
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {

        HashMap<Integer, Integer> mapVertex = new HashMap<>();
        ArrayList<Integer> visitList = new ArrayList<>();

        for (int i = 0; i < adjacencyMatrix.length; i++)
            mapVertex.put(i, Integer.MAX_VALUE);

        mapVertex.put(startIndex, 0);


        visitList.add(startIndex);
        while (visitList.size() != adjacencyMatrix.length) {
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (!visitList.contains(i) && i != startIndex && adjacencyMatrix[startIndex][i] > 0)
                    mapVertex.put(i, Math.min(mapVertex.get(i), mapVertex.get(startIndex) + adjacencyMatrix[startIndex][i]));
            }
            int min = Integer.MAX_VALUE;
            for (int i = 0; i < adjacencyMatrix.length; i++) {
                if (mapVertex.get(i) < min && i != startIndex && !visitList.contains(i)) {
                    min = mapVertex.get(i);
                    startIndex = i;
                }
            }
            visitList.add(startIndex);
        }
        return mapVertex;
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {

        Set<Integer> setList = new TreeSet<>();
        int n = adjacencyMatrix.length;
        int startVertex = 0;
        setList.add(startVertex);
        int result = 0;
        while (setList.size() != n) {
            int u = 0;
            int v = 0;
            int min = Integer.MAX_VALUE;

            for (Integer i : setList) {
                for (int j = 0; j < adjacencyMatrix[i].length; j++) {
                    int w = adjacencyMatrix[i][j];
                    if (w < min && w != 0 && (!setList.contains(j))) {
                        u = i;
                        v = j;
                        min = w;
                    }
                }
            }
            setList.add(u);
            setList.add(v);
            result += min;
        }

        return result;
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {

        ArrayList<Edge> edges = new ArrayList<>();

        int eCount = 0;

        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if ((i < j) && adjacencyMatrix[i][j] != 0) {
                    edges.add(new Edge(i, j, adjacencyMatrix[i][j]));
                    eCount++;
                }
            }
        }
        SetNumbers setNumbers = new SetNumbers(eCount);
        Collections.sort(edges);

        int result = 0;
        for (Edge edge : edges) {
            if (setNumbers.unification(edge.u, edge.v)) {
                result += edge.weight;
            }
        }
        return result;
    }


    public class SetNumbers {
        int[] setNum;

        SetNumbers(int size) {
            setNum = new int[size];
            for (int i = 0; i < size; i++) {
                setNum[i] = i;
            }
        }

        int getSet(int x) {
            if (x == setNum[x]) {
                return x;
            } else {
                setNum[x] = getSet(setNum[x]);
                return setNum[x];
            }
        }

        boolean unification(int u, int v) {
            u = getSet(u);
            v = getSet(v);
            if (u == v) return false;
            for (int i = 0; i < setNum.length; i++) {
                if (setNum[i] == v) {
                    setNum[i] = u;
                }
            }
            return true;
        }
    }

    public class Edge implements Comparable<Edge> {

        int u;
        int v;
        int weight;

        Edge(int u, int v, int w) {
            this.u = u;
            this.v = v;
            this.weight = w;
        }

        @Override
        public int compareTo(Edge edge) {
            if (weight != edge.weight) {
                return weight < edge.weight ? -1 : 1;
            }
            return 0;
        }
    }
}
